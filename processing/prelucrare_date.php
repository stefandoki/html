<?php

require "config.php";

$Message = $ErrorFirstName = $ErrorLastName = $ErrorEmail = $ErrorPassword = "";
$first_name = $last_name = $email = $password = "";

if (!empty($_POST)) {
    if (empty($_POST["first_name"])) {
        $ErrorFirstName = "First Name is required";
    } else {
        $first_name = sanitiseData($_POST["first_name"]);
        // check if name only contains letters and whitespace
        if (!preg_match("/^[a-zA-Z0-9_]*$/",$first_name)) {
            $ErrorFirstName = "Space and special characters not allowed but you can use underscore(_).";
        }
        else{
            $first_name=$first_name;
        }
    }

    if (empty($_POST["email"])) {
        $ErrorEmail = "Email is required";
    } else {
        $email = sanitiseData($_POST["email"]);
        // check if e-mail address is well-formed
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $ErrorEmail = "Invalid email format";
        }
        else{
            $email=$email;
        }
    }

    if ($ErrorFirstName!="" OR $ErrorLastName!="" OR $ErrorPassword!="" OR $ErrorEmail!=""){
        $Message = "Registration failed! Errors found";
        echo $Message;
        header("Location: index.html");
    }
    else{
        mysqli_query($conn,"INSERT INTO test.users (first_name, last_name, email, password) VALUES ('$first_name', '$last_name', '$email', '$password')");
        $Message = "Registration Successful!";
        echo $Message;
        header("Location: index.html");
    }
}

function sanitiseData($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}