<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Medical Vet - Prima pagina</title>
    <meta name="description" content="Adaugare pacienti">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="SEO,Pacienti,Cabinet,Veterinar">
    <meta name="author" content="Nume">

    <link rel="shortcut icon" href="/resources/images/favicon.ico" type="image/x-icon"/>

    <link rel="stylesheet" href="/resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="/resources/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/resources/css/linearicons.css">
    <link rel="stylesheet" href="/resources/css/magnific-popup.css">
    <link rel="stylesheet" href="/resources/css/animate.css">
    <!-- Main-Stylesheets -->
    <link rel="stylesheet" href="/resources/css/normalize.css">
    <link rel="stylesheet" href="/resources/css/style.css">
    <link rel="stylesheet" href="/resources/css/responsive.css">
    <script src="/resources/js/vendor/modernizr-2.8.3.min.js"></script>
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>
<body data-spy="scroll" data-target=".mainmenu-area">
<!-- Preloader-content -->
<div class="preloader">
    <span><i class="lnr lnr-paw"></i></span>
</div>

<nav class="mainmenu-area" data-spy="affix" data-offset-top="200">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#primary_menu">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/index.html"><img src="/resources/images/logo.png" alt="Logo"></a>
        </div>
        <div class="collapse navbar-collapse" id="primary_menu">
            <ul class="nav navbar-nav mainmenu">
                <li class="active"><a href="#home_page">Acasa</a></li>
                <li><a href="#about_page">Despre noi</a></li>
                <li><a href="#features_page">Features</a></li>
                <li><a href="#gallery_page">Galerie</a></li>
                <li><a href="#price_page">Preturi</a></li>
                <li><a href="#questions_page">FAQ</a></li>
                <li><a href="blog.html">Blog</a></li>
                <li><a href="#contact_page">Contact</a></li>
            </ul>
            <div class="right-button hidden-xs">
                <a href="/register.html">Inregistrare</a>
            </div>
        </div>
    </div>
</nav>

<header class="home-area overlay" id="home_page">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 hidden-sm col-md-5">
                <figure class="mobile-image wow fadeInUp" data-wow-delay="0.2s">
                    <img src="/resources/images/header-mobile.png" alt="">
                </figure>
            </div>
            <div class="col-xs-12 col-md-7">
                <div class="space-80 hidden-xs"></div>
                <h1 class="wow fadeInUp" data-wow-delay="0.4s">Clinica Veterinara ________</h1>
                <div class="space-20"></div>
                <div class="desc wow fadeInUp" data-wow-delay="0.6s">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiing elit, sed do eiusmod tempor incididunt ut labore
                        et laborused sed do eiusmod tempor incididunt ut labore et laborused.</p>
                </div>
                <div class="space-20"></div>
                <a href="#" class="bttn-white wow fadeInUp" data-wow-delay="0.8s"><i class="lnr lnr-phone-handset"></i>Contacteaza-ne!</a>
            </div>
        </div>
    </div>
</header>
<!-- Home-Area-End -->
<!-- About-Area -->
<section class="section-padding" id="about_page">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-10 col-md-offset-1">
                <div class="page-title text-center">
                    <img src="/resources/images/about-logo.png" alt="About Logo">
                    <div class="space-20"></div>
                    <h5 class="title">Despre Noi</h5>
                    <div class="space-30"></div>
                    <h3 class="blue-color">Lorem ipsum dolor sit amet, consectetur adipiing elit, sed do eiusmod tempor
                        incididunt ut labore et laborused sed do eiusmod tempor incididunt ut labore et laborused. </h3>
                    <div class="space-20"></div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiing elit, sed do eiusmod tempor incididunt ut labore
                        et laborused sed do eiusmod tempor incididunt ut labore et laborused.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- About-Area-End -->
<!-- Progress-Area -->
<section class="progress-area gray-bg" id="progress_page">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <div class="page-title section-padding">
                    <h5 class="title wow fadeInUp" data-wow-delay="0.2s">Activitatea Noastra</h5>
                    <div class="space-10"></div>
                    <h3 class="dark-color wow fadeInUp" data-wow-delay="0.4s">Lorem ipsum</h3>
                    <div class="space-20"></div>
                    <div class="desc wow fadeInUp" data-wow-delay="0.6s">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiing elit, sed do eiusmod tempor incididunt ut
                            labore et laborused sed do eiusmod tempor incididunt ut labore et laborused.</p>
                    </div>
                    <div class="space-50"></div>
                    <a href="#" class="bttn-default wow fadeInUp" data-wow-delay="0.8s"><i
                            class="lnr lnr-arrow-right-circle"></i>Afla mai multe</a>
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <figure class="mobile-image">
                    <img src="/resources/images/progress-phone.png" alt="">
                </figure>
            </div>
        </div>
    </div>
</section>
<!-- Progress-Area-End -->
<!-- Video-Area -->
<section class="video-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <div class="video-photo">
                    <img src="/resources/images/video-image.png" alt="">
                    <a href="https://www.youtube.com/watch?v=SrOiKkFsGoM" class="popup video-button">
                        <img src="/resources/images/play-button.png" alt="">
                    </a>
                </div>
            </div>
            <div class="col-xs-12 col-md-5 col-md-offset-1">
                <div class="space-60 hidden visible-xs"></div>
                <div class="page-title">
                    <h5 class="title wow fadeInUp" data-wow-delay="0.2s">Program Flexibil</h5>
                    <div class="space-10"></div>
                    <h3 class="dark-color wow fadeInUp" data-wow-delay="0.4s">Lorem ipsum</h3>
                    <div class="space-20"></div>
                    <div class="desc wow fadeInUp" data-wow-delay="0.6s">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiing elit, sed do eiusmod tempor incididunt ut
                            labore et laborused sed do eiusmod tempor incididunt ut labore et laborused.</p>
                    </div>
                    <div class="space-50"></div>
                    <a href="#" class="bttn-default wow fadeInUp" data-wow-delay="0.8s"><i class="lnr lnr-alarm"></i>Vezi
                        programul complet</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Video-Area-End -->
<!-- Feature-Area -->
<section class="feature-area section-padding-top" id="features_page">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <div class="page-title text-center">
                    <h5 class="title">Beneficii</h5>
                    <div class="space-10"></div>
                    <h3>Lorem ipsum dolor</h3>
                    <div class="space-60"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="service-box wow fadeInUp" data-wow-delay="0.2s">
                    <div class="box-icon">
                        <i class="lnr lnr-rocket"></i>
                    </div>
                    <h4>Rapiditate</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
                <div class="space-60"></div>
                <div class="service-box wow fadeInUp" data-wow-delay="0.4s">
                    <div class="box-icon">
                        <i class="lnr lnr-paperclip"></i>
                    </div>
                    <h4>Profesionalism</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
                <div class="space-60"></div>
                <div class="service-box wow fadeInUp" data-wow-delay="0.6s">
                    <div class="box-icon">
                        <i class="lnr lnr-cloud-download"></i>
                    </div>
                    <h4>Lorem ipsum</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
                <div class="space-60"></div>
            </div>
            <div class="hidden-xs hidden-sm col-md-4">
                <figure class="mobile-image">
                    <img src="/resources/images/feature-image.png" alt="Feature Photo">
                </figure>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="service-box wow fadeInUp" data-wow-delay="0.2s">
                    <div class="box-icon">
                        <i class="lnr lnr-clock"></i>
                    </div>
                    <h4>Comunicare</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
                <div class="space-60"></div>
                <div class="service-box wow fadeInUp" data-wow-delay="0.4s">
                    <div class="box-icon">
                        <i class="lnr lnr-laptop-phone"></i>
                    </div>
                    <h4>Lorem ipsum</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
                <div class="space-60"></div>
                <div class="service-box wow fadeInUp" data-wow-delay="0.6s">
                    <div class="box-icon">
                        <i class="lnr lnr-cog"></i>
                    </div>
                    <h4>Lorem ipsum</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
                <div class="space-60"></div>
            </div>
        </div>
    </div>
</section>
<!-- Feature-Area-End -->
<!-- Testimonial-Area -->
<section class="testimonial-area" id="testimonial_page">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title text-center">
                    <h5 class="title">Parerile clientilor</h5>
                    <h3 class="dark-color">Lorem ipsum</h3>
                    <div class="space-60"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="team-slide">
                    <div class="team-box">
                        <div class="team-image">
                            <img src="/resources/images/team-1.png" alt="">
                        </div>
                        <h4>Lorem ipsum</h4>
                        <h6 class="position">Functie</h6>
                        <p>Lorem ipsum dolor sit amet, conseg sed do eiusmod temput laborelaborus ed sed do eiusmod
                            tempo.</p>
                    </div>
                    <div class="team-box">
                        <div class="team-image">
                            <img src="/resources/images/team-1.png" alt="">
                        </div>
                        <h4>Lorem ipsum</h4>
                        <h6 class="position">Functie</h6>
                        <p>Lorem ipsum dolor sit amet, conseg sed do eiusmod temput laborelaborus ed sed do eiusmod
                            tempo.</p>
                    </div>
                    <div class="team-box">
                        <div class="team-image">
                            <img src="/resources/images/team-1.png" alt="">
                        </div>
                        <h4>Lorem ipsum</h4>
                        <h6 class="position">Functie</h6>
                        <p>Lorem ipsum dolor sit amet, conseg sed do eiusmod temput laborelaborus ed sed do eiusmod
                            tempo.</p>
                    </div>
                    <div class="team-box">
                        <div class="team-image">
                            <img src="/resources/images/team-1.png" alt="">
                        </div>
                        <h4>Lorem ipsum</h4>
                        <h6 class="position">Functie</h6>
                        <p>Lorem ipsum dolor sit amet, conseg sed do eiusmod temput laborelaborus ed sed do eiusmod
                            tempo.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Testimonial-Area-End -->
<!-- Gallery-Area -->
<section class="gallery-area section-padding" id="gallery_page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-6 gallery-slider">
                <div class="gallery-slide">
                    <div class="item"><img src="/resources/images/gallery-1.jpg" alt=""></div>
                    <div class="item"><img src="/resources/images/gallery-2.jpg" alt=""></div>
                    <div class="item"><img src="/resources/images/gallery-3.jpg" alt=""></div>
                    <div class="item"><img src="/resources/images/gallery-4.jpg" alt=""></div>
                    <div class="item"><img src="/resources/images/gallery-1.jpg" alt=""></div>
                    <div class="item"><img src="/resources/images/gallery-2.jpg" alt=""></div>
                    <div class="item"><img src="/resources/images/gallery-3.jpg" alt=""></div>
                    <div class="item"><img src="/resources/images/gallery-1.jpg" alt=""></div>
                    <div class="item"><img src="/resources/images/gallery-2.jpg" alt=""></div>
                    <div class="item"><img src="/resources/images/gallery-3.jpg" alt=""></div>
                    <div class="item"><img src="/resources/images/gallery-4.jpg" alt=""></div>
                    <div class="item"><img src="/resources/images/gallery-1.jpg" alt=""></div>
                    <div class="item"><img src="/resources/images/gallery-2.jpg" alt=""></div>
                    <div class="item"><img src="/resources/images/gallery-3.jpg" alt=""></div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-5 col-lg-3">
                <div class="page-title">
                    <h5 class="white-color title wow fadeInUp" data-wow-delay="0.2s">Pacienti</h5>
                    <div class="space-10"></div>
                    <h3 class="white-color wow fadeInUp" data-wow-delay="0.4s">Pacientii nostrii</h3>
                </div>
                <div class="space-20"></div>
                <div class="desc wow fadeInUp" data-wow-delay="0.6s">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiing elit, sed do eiusmod tempor incididunt ut labore
                        et laborused sed do eiusmod tempor incididunt ut labore et laborused.</p>
                </div>
                <div class="space-50"></div>
                <a href="#" class="bttn-default wow fadeInUp" data-wow-delay="0.8s"><i class="lnr lnr-camera"></i>Vezi
                    mai multe</a>
            </div>
        </div>
    </div>
</section>
<!-- Gallery-Area-End -->
<!-- How-To-Use -->
<section class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="page-title">
                    <h5 class="title wow fadeInUp" data-wow-delay="0.2s">Our features</h5>
                    <div class="space-10"></div>
                    <h3 class="dark-color wow fadeInUp" data-wow-delay="0.4s">Aour Approach of Design is Prety Simple
                        and Clear</h3>
                </div>
                <div class="space-20"></div>
                <div class="desc wow fadeInUp" data-wow-delay="0.6s">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiing elit, sed do eiusmod tempor incididunt ut labore
                        et laborused sed do eiusmod tempor incididunt ut labore et laborused.</p>
                </div>
                <div class="space-50"></div>
                <a href="#" class="bttn-default wow fadeInUp" data-wow-delay="0.8s">Learn More</a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1">
                <div class="space-60 hidden visible-xs"></div>
                <div class="service-box wow fadeInUp" data-wow-delay="0.2s">
                    <div class="box-icon">
                        <i class="lnr lnr-clock"></i>
                    </div>
                    <h4>Easy Notifications</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                </div>
                <div class="space-50"></div>
                <div class="service-box wow fadeInUp" data-wow-delay="0.2s">
                    <div class="box-icon">
                        <i class="lnr lnr-laptop-phone"></i>
                    </div>
                    <h4>Fully Responsive</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                </div>
                <div class="space-50"></div>
                <div class="service-box wow fadeInUp" data-wow-delay="0.2s">
                    <div class="box-icon">
                        <i class="lnr lnr-cog"></i>
                    </div>
                    <h4>Editable Layout</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- How-To-Use-End -->
<!-- Download-Area -->
<div class="download-area overlay">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 hidden-sm">
                <figure class="mobile-image">
                    <img src="/resources/images/download-image.png" alt="">
                </figure>
            </div>
            <div class="col-xs-12 col-md-6 section-padding">
                <h3 class="white-color">Descarca aplicatia!</h3>
                <div class="space-20"></div>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam possimus eaque magnam eum
                    praesentium unde.</p>
                <div class="space-60"></div>
                <a href="#" class="bttn-white sq"><img src="/resources/images/apple-icon.png" alt="apple icon"> Apple Store</a>
                <a href="#" class="bttn-white sq"><img src="/resources/images/play-store-icon.png" alt="Play Store Icon"> Play
                    Store</a>
            </div>
        </div>
    </div>
</div>
<!-- Download-Area-End -->
<!--Price-Area -->
<section class="section-padding price-area" id="price_page">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title text-center">
                    <h5 class="title">Preturi</h5>
                    <h3 class="dark-color">Lista de preturi orientative</h3>
                    <div class="space-60"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <div class="price-box">
                    <div class="price-header">
                        <div class="price-icon">
                            <span class="lnr lnr-paw"></span>
                        </div>
                        <h4 class="upper">Gratuit</h4>
                    </div>
                    <div class="price-body">
                        <ul>
                            <li>Primul consult</li>
                            <li>Carnetul de sanatate</li>
                            <li>Vaccin</li>
                        </ul>
                    </div>
                    <div class="price-rate">
                        <sup>Lei</sup> <span class="rate">0</span>
                    </div>
                    <div class="price-footer">
                        <a href="#" class="bttn-white">Rezerva!</a>
                    </div>
                </div>
                <div class="space-30 hidden visible-xs"></div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="price-box">
                    <div class="price-header">
                        <div class="price-icon">
                            <span class="lnr lnr-diamond"></span>
                        </div>
                        <h4 class="upper">Medium</h4>
                    </div>
                    <div class="price-body">
                        <ul>
                            <li>Lorem impsum</li>
                            <li>Lorem impsum</li>
                            <li>Lorem impsum</li>
                        </ul>
                    </div>
                    <div class="price-rate">
                        <sup>Lei</sup> <span class="rate">50</span> <small>/Luna</small>
                    </div>
                    <div class="price-footer">
                        <a href="#" class="bttn-white">Rezerva!</a>
                    </div>
                </div>
                <div class="space-30 hidden visible-xs"></div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="price-box">
                    <div class="price-header">
                        <div class="price-icon">
                            <span class="lnr lnr-history"></span>
                        </div>
                        <h4 class="upper">Premium</h4>
                    </div>
                    <div class="price-body">
                        <ul>
                            <li>Consult periodic</li>
                            <li>Assistenta 24/7</li>
                            <li>Prioritate</li>
                        </ul>
                    </div>
                    <div class="price-rate">
                        <sup>Lei</sup> <span class="rate">100</span> <small>/Luna</small>
                    </div>
                    <div class="price-footer">
                        <a href="#" class="bttn-white">Rezerva!</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Price-Area-End -->
<!--Questions-Area -->
<section id="questions_page" class="questions-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title text-center">
                    <h5 class="title">FAQ</h5>
                    <h3 class="dark-color">Intrebari frecvente</h3>
                    <div class="space-60"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="toggole-boxs">
                    <h3>FAQ - Prima intrebare. </h3>
                    <div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, ullamco laboris nisi ut aliquip ex
                            ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                            dolore eu fugiat nulla pariatur.</p>
                    </div>
                    <h3>Lorem ipsum dolor sit amet? </h3>
                    <div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, ullamco laboris nisi ut aliquip ex
                            ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                            dolore eu fugiat nulla pariatur.</p>
                    </div>
                    <h3>Lorem ipsum dolor sit amet? </h3>
                    <div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, ullamco laboris nisi ut aliquip ex
                            ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                            dolore eu fugiat nulla pariatur.</p>
                    </div>
                    <h3>Lorem ipsum dolor sit amet? </h3>
                    <div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, ullamco laboris nisi ut aliquip ex
                            ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                            dolore eu fugiat nulla pariatur.</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="space-20 hidden visible-xs"></div>
                <div class="toggole-boxs">
                    <h3>FAQ - A doua intrebare?</h3>
                    <div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, ullamco laboris nisi ut aliquip ex
                            ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                            dolore eu fugiat nulla pariatur.</p>
                    </div>
                    <h3>Lorem ipsum dolor sit amet? </h3>
                    <div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, ullamco laboris nisi ut aliquip ex
                            ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                            dolore eu fugiat nulla pariatur.</p>
                    </div>
                    <h3>Lorem ipsum dolor sit amet? </h3>
                    <div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, ullamco laboris nisi ut aliquip ex
                            ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                            dolore eu fugiat nulla pariatur.</p>
                    </div>
                    <h3>Lorem ipsum dolor sit amet? </h3>
                    <div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, ullamco laboris nisi ut aliquip ex
                            ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                            dolore eu fugiat nulla pariatur.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Questions-Area-End -->
<!-- Subscribe-Form -->
<div class="subscribe-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <div class="subscribe-form text-center">
                    <h3 class="blue-color">Aboneaza-te la newsletter si beneficiaza de oferte!</h3>
                    <div class="space-20"></div>
                    <form id="mc-form">
                        <input type="email" class="control" placeholder="Introdu adresa de email" required="required"
                               id="mc-email">
                        <button class="bttn-white active" type="submit"><span class="lnr lnr-location"></span> Abonare
                        </button>
                        <label class="mt10" for="mc-email"></label>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Subscribe-Form-Area -->
<!-- Footer-Area -->
<footer class="footer-area" id="contact_page">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title text-center">
                        <h5 class="title">Contacteaza-ne!</h5>
                        <h3 class="dark-color">Ne gasesti la</h3>
                        <div class="space-60"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <div class="footer-box">
                        <div class="box-icon">
                            <span class="lnr lnr-map-marker"></span>
                        </div>
                        <p>Locatie <br/> Oras</p>
                    </div>
                    <div class="space-30 hidden visible-xs"></div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="footer-box">
                        <div class="box-icon">
                            <span class="lnr lnr-phone-handset"></span>
                        </div>
                        <p>+40 000 000 00 <br/> +407 000 000 00</p>
                    </div>
                    <div class="space-30 hidden visible-xs"></div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="footer-box">
                        <div class="box-icon">
                            <span class="lnr lnr-envelope"></span>
                        </div>
                        <p>adresa@email.com <br/> mail@email.com
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer-Bootom -->
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-5">
                    <div class="space-30 hidden visible-xs"></div>
                </div>
                <div class="col-xs-12 col-md-7">
                    <div class="footer-menu">
                        <ul>
                            <li><a href="#about_page">Despre noi</a></li>
                            <li><a href="#features_page">Features</a></li>
                            <li><a href="#gallery_page">Galerie</a></li>
                            <li><a href="#price_page">Preturi</a></li>
                            <li><a href="#questions_page">FAQ</a></li>
                            <li><a href="blog.html">Blog</a></li>
                            <li><a href="#contact_page">Contact</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer-Bootom-End -->
</footer>
<!-- Footer-Area-End -->
<!--Vendor-JS-->
<script src="/resources/js/vendor/jquery-1.12.4.min.js"></script>
<script src="/resources/js/vendor/jquery-ui.js"></script>
<script src="/resources/js/vendor/bootstrap.min.js"></script>
<!--Plugin-JS-->
<script src="/resources/js/owl.carousel.min.js"></script>
<script src="/resources/js/contact-form.js"></script>
<script src="/resources/js/ajaxchimp.js"></script>
<script src="/resources/js/scrollUp.min.js"></script>
<script src="/resources/js/magnific-popup.min.js"></script>
<script src="/resources/js/wow.min.js"></script>
<!--Main-active-JS-->
<script src="/resources/js/main.js"></script>
</body>
</html>